# devops-project-work
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=kapseliboi_devops-project&metric=alert_status)](https://sonarcloud.io/dashboard?id=kapseliboi_devops-project)

COMP.SE.140 Devops course project assigment

Repository contains the application and pipeline setup for the project.

End report is provided in EndReport.pdf file.

### Instructions for the examiner 

To get our application up and running one has to simply have Docker compose installed and run docker-compose up in the root directory of our project. This will deploy our whole project into Docker containers locally. If running tests is required, we recommend installing .NET Core SDK 3.1 or using a Gitlab CI and Runner to run the tests for you.  

We have not defined a way to execute the test locally with Docker because we haven’t needed that sort of feature. To run the tests locally just run dotnet test in the root directory of the project. If you are using Visual Studio, you can load the Solution file and execute the tests using the Visual Studio Unit testing tools, also, which is visually much more pleasant way to test the project, but of course requires Visual Studio with correct .NET tools to be installed.  

When changing the system states using PUT requests to the API gateway /state endpoint, there is a special requirement to use “JSON” format for the request body, in other words, the request should have “Content-type”-parameter set to “application/json”. The request body payload should be just plain JSON string, e.g. “INIT”, “PAUSED”, “SHUTDOWN” or “RUNNING”, with quotes included. 

Example request for changing service state to PAUSED with cURL: curl -X PUT -H "content-type: application/json" -d "\"PAUSED\"" localhost:8081/state 

A remote version of the application is running in the public cloud at https://devops.peltonet.com/. Be careful! If you send the SHUTDOWN state to our service there is absolutely nothing you can do to get it back up again as the instructions clearly stated that all containers should be powered off so this also includes ApiGateway. To get it back up running again, would require another deployment using the pipeline. Our repository, source code and collaboration history can be found here: https://gitlab.com/kapseliboi/devops-project  

and our SonarQube analysis report page can be found here: https://sonarcloud.io/dashboard?id=kapseliboi_devops-project. 

Implemented features / features to test: 

- Continuous integration (.gitlab-ci.yml) 
- Automated testing framework 
- Changes to the application (API Gateway) 
- Static analysis (SonarQube) 
- Deployment to external cloud (available through URL) 
- Node-statistic –endpoint (API Gateway) 
- Separate monitoring / logging service (Seq) 
- End report

In order to test the automated testing framework you need to have .NET Core SDK 3.1 installed as described earlier. ApiGateway can be tested by just using Docker compose to run the system. The static analysis can be hard to test. It would require us to invite you to our project and to give examiner rights to run tests. We think that this is pretty much unnecessary because you have the whole history of our test runs available to you but if the examiner requires access to our repository with more rights then please contact us. Our separate monitoring / logging service can be troubleshooted at URL https://logger.peltonet.com and locally at http://localhost:5341/. 